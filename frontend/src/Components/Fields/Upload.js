import { getBase64 } from "../../helpers/ImgHelper";
import style from './upload.module.css'

function Uploads({ value, changeHandler = () => {} }) {

    const onChangeHandler = (e) => {
        if (!e.target.files[0]) return;
        getBase64(e?.target?.files[0], (result) => {
            changeHandler(result)
        });
    }

    return (
        <div>
            <label className={style.image}>
                <img src={value || 'Avatar.png'} alt=""/>
                <p className={style.upload_text}>Upload</p>
                <input
                    onChange={onChangeHandler}
                    type={'file'}
                    className={style.visible}
                />
            </label>
        </div>
    );
}

export default Uploads;
