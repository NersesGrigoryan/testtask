import style from './logo.module.css'

function Logo() {
    return (
        <div className={`${style.mb_50} ${style.mt_50} ${style.flex}`}>
            <img src="Logo.png" alt=""/>
        </div>
    );
}

export default Logo;
