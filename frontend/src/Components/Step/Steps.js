import Step from "./StepComponent";
import stepClasses from './steps.module.css';

function Steps() {
    return (
        <div className={stepClasses.item}>
            <div className={stepClasses.line}></div>
            <div className={stepClasses.all_steps}>
                <Step number={1} status={true} text={'CREATE YOUR ACCOUNT PASSWORD'}></Step>
                <Step number={2}  text={'PERSONAL INFORMATION'}></Step>
                <Step number={3}  text={'EMPLOYMENT DETAILS'}></Step>
                <Step number={4}  text={'UPLOAD DOCUMENTS'}></Step>
                <Step number={5}  text={'COMPLETE'}></Step>
            </div>
        </div>
    );
}

export default Steps;
