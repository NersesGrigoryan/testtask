import stepClasses from './step.module.css';

function Step(props) {
    return (
        <div className={stepClasses.item}>
            <div className={`${props.status?stepClasses.active:''} ${stepClasses.circle}`}>
                {props.number}
            </div>
            <div>
                <p className={`${stepClasses.header_text} ${props.status?stepClasses.active_text:''} ${stepClasses.text_style}`}>
                    Step {props.number} :
                </p>
                <p className={`${props.status?stepClasses.active_text:''}  ${stepClasses.text_style}`}>
                    {props.text}
                </p>
            </div>
        </div>
    );
}

export default Step;
