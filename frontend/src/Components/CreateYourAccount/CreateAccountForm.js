import {useState} from 'react'
import axios from "axios";
import Field from "../Fields/Field";
import style from './create-account.module.css';
import Uploads from "../Fields/Upload";
import {toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

function CreateAccountForm() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [file, setFile] = useState('');

    let createAccount = async () => {
        const data = {
            username:name,
            email,
            password,
            confirm_password:confirmPassword,
            avatar:file
        }
        try {
            const res = await axios.post(`http://localhost:3000/account`, data);
            if (res) toast.success(res.data.message)
        } catch (e) {
            toast.error(e.response.data.message)
        }
    }
    return (
        <div>
            <div className={style.fields_main}>
                <Uploads
                    value={file}
                    changeHandler={setFile}
                />
                <div className={style.fields_parent}>
                    <div className={style.fields_flex}>
                        <Field
                            label={'NAME'}
                            type={'text'}
                            value={name}
                            changeHandler={setName}
                        />
                        <Field
                            label={'EMAIL'}
                            type={'email'}
                            value={email}
                            changeHandler={setEmail}
                        />
                    </div>
                    <div className={style.fields_flex}>
                        <Field
                            label={'PASSWORD'}
                            type={'password'}
                            value={password}
                            changeHandler={setPassword}
                        />
                        <Field
                            label={'CONFIRM PASSWORD'}
                            type={'password'}
                            value={confirmPassword}
                            changeHandler={setConfirmPassword}
                        />
                    </div>
                </div>
            </div>
            <div className={style.btn}>
                <button onClick={createAccount}>
                    <span>SAVE & NEXT</span>
                    <img src="arrow-right.png" alt=""/>
                </button>
            </div>
        </div>
    );
}

export default CreateAccountForm;
