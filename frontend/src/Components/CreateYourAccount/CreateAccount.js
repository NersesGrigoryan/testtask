import style from './create-account.module.css';
import Logo from "../Logo/Logo";
import Steps from "../Step/Steps";
import CreateAccountForm from "./CreateAccountForm";
import {ToastContainer} from "react-toastify";

function CreateAccount() {
    return (
        <div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <ToastContainer />
            <Logo/>
            <Steps/>
            <div>
                <p className={style.title}>CREATE YOUR ACCOUNT</p>
                <p className={style.text}>Because there will be documents that you need to prepare to apply for the loan,
                    let's start off by creating a password so that you can login to your account once you have these document ready.</p>
            </div>
            <CreateAccountForm/>
        </div>
    );
}

export default CreateAccount;
