export const getBase64 = (item, cb) => {
    let reader = new FileReader();
    reader.readAsDataURL(item);
    reader.onload = function () {
        cb(reader.result)
    };
    reader.onerror = function (error) {};
}